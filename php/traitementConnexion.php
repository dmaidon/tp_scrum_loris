<?php
session_start();
require 'BDD.php';

if ((!empty($_POST['email']))&&(!empty($_POST['password']))){
	
	$username = htmlspecialchars($_POST['email']);
	$password = htmlspecialchars($_POST['password']);
	$req = $bdd->prepare('SELECT * FROM utilisateur WHERE email = :email'); // On récup l'email et le hash du MDP correspondant à l'utilisateur dans la BDD
	$req->execute(array(
		'email'=>$username,	
	));
	$utilisateur = $req->fetch(); 
	$mdp = $utilisateur["motdepasse"];

	//Recupérer le role pour afficher certaines parties

	if(password_verify($password, $mdp)){
			$_SESSION['user'] = $utilisateur['email'];
			$_SESSION['role'] = $utilisateur['role'];
			header('Location:accueil.php');

	}

	else{
				
		$message='Identifiant ou mot de passe incorrect';
		echo '<script type="text/javascript">window.alert("'.$message.'");window.location.replace("PageConnexion.php");</script>';
	}
}
