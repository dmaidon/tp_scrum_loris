<?php

session_start();

include 'BDD.php';

?>


<!doctype html>

<html lang="fr">

  <head>

    <meta charset="utf-8">


    <meta name="viewport" content="width=device-width, initial-scale=1">


   
    <link rel="icon" type="image/png" sizes="16x16" href="ldc.png">

    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/starter-template/">

    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"

     integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" 

  integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>



   

    
    
  </head>

  <body>


  <nav class="navbar navbar-expand-md navbar-dark mb-4" style="background-color: #C0C0C0  opacity : 0,5 ;" >

  <div class="container-fluid">

    
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">

      <span class="navbar-toggler-icon"></span>

    </button>


    <div class="collapse navbar-collapse" id="navbarsExampleDefault">

      <ul class="navbar-nav me-auto mb-2 mb-md-0">

      <li class="nav-item <?php if ($_SERVER['SCRIPT_NAME'] === 'acceuil.php'): ?>active<?php endif; ?>">

          <a class="nav-link" aria-current="page" href="accueil.php">Accueil</a>

      </li>

      <?php if(empty(($_SESSION['user']))){

        ?>

      <li class="nav-item <?php if ($_SERVER['SCRIPT_NAME'] === ''): ?>active<?php endif; ?>">

          <a class="nav-link" aria-current="page" href="Inscription.php">Inscription</a>

      </li>

      
      <li class="nav-item <?php if ($_SERVER['SCRIPT_NAME'] === 'PageConnexion.php'): ?>active<?php endif; ?>">

          <a class="nav-link" aria-current="page" href="PageConnexion.php">Connexion</a>

      </li>

      <li class="nav-item <?php if ($_SERVER['SCRIPT_NAME'] === 'affichageRègles.php'): ?>active<?php endif; ?>">

      <a class="nav-link" aria-current="page" href="affichageRègles.php">Règles</a>

  </li>

      <li class="nav-item <?php if ($_SERVER['SCRIPT_NAME'] === 'affichageScore.php'): ?>active<?php endif; ?>">

          <a class="nav-link" aria-current="page" href="affichageScore.php">Résultat</a>

      </li>

      <li class="nav-item <?php if ($_SERVER['SCRIPT_NAME'] === 'championnat.php'): ?>active<?php endif; ?>">

          <a class="nav-link" aria-current="page" href="championnat.php">Championnat/Tournoi</a>

      </li>

      <?php

      }

      ?>


<?php


if(!empty($_SESSION['user'])){

  ?>

   <li class="nav-item <?php if ($_SERVER['SCRIPT_NAME'] === 'affichageRègles.php'): ?>active<?php endif; ?>">

      <a class="nav-link" aria-current="page" href="affichageRègles.php">Règles</a>

  </li>

  <!--<li class="nav-item <?php if ($_SERVER['SCRIPT_NAME'] === 'affichageScore.php'): ?>active<?php endif; ?>">

      <a class="nav-link" aria-current="page" href="affichageScore.php">Résultat</a>

  </li>-->

  <li class="nav-item <?php if ($_SERVER['SCRIPT_NAME'] === 'championnat.php'): ?>active<?php endif; ?>">

          <a class="nav-link" aria-current="page" href="championnat.php">Championnat/Coupe</a>

      </li>

  <?php

  $username = $_SESSION['user'];


  $req = $bdd->prepare('SELECT * FROM utilisateur WHERE email = :email'); //On récup l'email et le hash du MDP correspondant à l'utilisateur dans la BDD

            $req->execute(array(

                'email'=>$username,    

            ));

            $utilisateur = $req->fetch();

            
            //Recupérer le role pour afficher certaines parties

            $role = $_SESSION['role'];




      if(($_SESSION['role']==1)||($_SESSION['role']==2)){ 

        ?>

        <li class="nav-item <?php if ($_SERVER['SCRIPT_NAME'] === ''): ?>active<?php endif; ?>">

          <a class="nav-link" aria-current="page" href="inscriptionEquipe1.php">Inscrire votre équipe</a>

      </li>

      <li class="nav-item <?php if ($_SERVER['SCRIPT_NAME'] === ''): ?>active<?php endif; ?>">

          <a class="nav-link" aria-current="page" href="pushRandom.php">Remplissage aléatoire (Test)</a>

      </li>

          <?php

      }

     
      if($_SESSION['role']==2){

        ?>

      <li class="nav-item <?php if ($_SERVER['SCRIPT_NAME'] === ''): ?>active<?php endif; ?>">

          <a class="nav-link" aria-current="page" href="creationTournois.php">Créer un tournoi</a>

      </li>

        <?php

      }

      ?>


<li class="nav-item <?php if ($_SERVER['SCRIPT_NAME'] === ''): ?>active<?php endif; ?>">

          <a class="nav-link" aria-current="page" href="deconnexion.php">Se déconnecter</a>

      </li>

      </ul>

     
    </div>

  </div>

  <?php

}

?>

</nav>



<main class="container">
